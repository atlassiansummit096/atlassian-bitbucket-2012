#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""Python Sudoku is a text and graphical program (gtk interface) to create or
resolve sudokus. It can also print a sudoku (1 or 4 sudokus in each page) and
write an image (png, jpeg, etc) with a sudoku.

For usage: pysdk.py --help


Copyright (C) 2005-2008  Xosé Otero <xoseotero@users.sourceforge.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


Modification history:
2005/10/12  Antti Kuntsi
	Added options to set the region size.

"""

import sys
import os
import traceback
import optparse
# Python Sudoku internacionalization
import locale
import gettext


import codecs

# Wrap sys.stdout and sys.stderr is needed for optparse that use
# sys.stdout.write instead of print
# This was fixed in python version 2.5 and is not needed any more.
if sys.version_info[0] <= 2 and sys.version_info[1] <= 4:
    if sys.stdout.encoding:
        sys.stdout = codecs.getwriter(sys.stdout.encoding)(sys.stdout)
    if sys.stderr.encoding:
        sys.stderr = codecs.getwriter(sys.stderr.encoding)(sys.stderr)

# Wrap sys.stdout and sys.stderr is needed when is redirected
if not sys.stdout.encoding:
    sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)
if not sys.stderr.encoding:
    sys.stderr = codecs.getwriter(locale.getpreferredencoding())(sys.stderr)


from pythonsudoku.config import localedir, options
locale.setlocale(locale.LC_ALL, '')
l10n = gettext.translation("pythonsudoku", localedir, fallback=True)
l10n.install(True)


from pythonsudoku.text import create_sudoku, solve_sudoku, test_difficulty
from pythonsudoku.info import Info


# send bugs here
BUG_URL = "http://sourceforge.net/tracker/?group_id=150356&atid=778307"


def whatis():
    """Print what is a sudoku."""
    print Info["whatis"]

def create_parser():
    """Create a optparse.OptionParser with all the Python Sudoku options."""
    usage = _(u"""
  %prog INPUT.sdk [OUTPUT.sdk]
  %prog -c [Creation Options] OUTPUT.sdk
  %prog -t INPUT.sdk
  %prog --version | -h | -m | -w

  INPUT.sdk and OUTPUT.sdk are Python Sudoku files""")
    version = "%prog " + Info["version"]
    parser = optparse.OptionParser(usage=usage, version=version)
    parser.add_option("-c", "--create",
                      action="store_true", dest="create", default=False,
                      help=_(u"create a sudoku"))

    parser.add_option("-t", "--test",
                      action="store_true", dest="test", default=False,
                      help=_(u"test the difficulty of a sudoku"))

    parser.add_option("-w", "--whatis",
                      action="store_true", dest="info", default=False,
                      help=_(u"information about what is sudoku"))

    group = optparse.OptionGroup(parser,
                                 _(u"Creation Options"))
    group.add_option("--difficulty",
                     action="store", type="string", dest="difficulty",
                     default=options.get("sudoku", "difficulty"),
                     help=_(u"set the difficulty of the sudoku (\"hard\", \"normal\", \"easy\") (\"%s\" is the default)") % options.get("sudoku", "difficulty"))
    group.add_option("--force",
                     action="store_true", dest="force", default=False,
                     help=_(u"be sure that the difficulty of the sudoku created is the difficulty given"))
    group.add_option("--handicap",
                     action="store", type="int", dest="handicap",
                     default=options.getint("sudoku", "handicap"),
                     help=_(u"set the handicap of the sudoku (0 = insane, 1 = insane + 1 extra number, etc) (%d is the default)") % options.getint("sudoku", "handicap"))
    group.add_option("--region_width",
                     action="store", type="int", dest="region_width",
                     default=options.getint("sudoku", "region_width"),
                     help=_(u"set the region width. The board will be HxW grid of WxH grids (%d is the default)") % options.getint("sudoku", "region_width"))
    group.add_option("--region_height",
                     action="store", type="int", dest="region_height",
                     default=options.getint("sudoku", "region_height"),
                     help=_(u"set the region height. The board will be HxW grid of WxH grids (%d is the default)") % options.getint("sudoku", "region_height"))
    parser.add_option_group(group)

    group = optparse.OptionGroup(parser, _(u"Visualization Options"))
    if options.getboolean("sudoku", "use_letters"):
        help_use_letters = _(u"show letters instead of numbers > 9 (default)")
        help_numbers_only = _(u"show only numbers")
    else:
        help_use_letters = _(u"show letters instead of numbers > 9")
        help_numbers_only = _(u"show only numbers (default)")
    group.add_option("--use_letters",
                     action="store_true", dest="use_letters",
                     default=options.getboolean("sudoku", "use_letters"),
                     help=help_use_letters)
    group.add_option("--numbers_only",
                     action="store_false", dest="use_letters",
                     default=not options.getboolean("sudoku", "use_letters"),
                     help=help_numbers_only)
    parser.add_option_group(group)

    return parser

def check_args(parser, options, args):
    """Check if the number of arguments if correct.

    Arguments:
    parser -- the optparse.OptionParser
    options -- then options returned by optparse.OptionParser.parser_args()
    args -- the args returned by options returned by
            optparse.OptionParser.parser_args()

    """
    length = len(args)

    if (options.create and (length != 1)) or \
       (options.test and (length != 1)) or \
       (length < 1 or length > 2):
        parser.error(_(u"incorrect number of arguments"))

def set_options(opts):
    global options

    # 3rd param of SafeConfigParser.set() must be a string
    options.set("sudoku", "difficulty", opts.difficulty)
    options.set("sudoku", "force", str(opts.force))
    options.set("sudoku", "handicap", str(opts.handicap))
    options.set("sudoku", "region_height", str(opts.region_height))
    options.set("sudoku", "region_width", str(opts.region_width))
    options.set("sudoku", "use_letters", str(opts.use_letters))


def main():
    parser = create_parser()
    (opts, args) = parser.parse_args()
    check_args(parser, opts, args)
    set_options(opts)

    try:
        import psyco
        psyco.full()
    except ImportError:
        pass

    status = 0

    if opts.create:
        status = not create_sudoku(args[0])
    elif opts.test:
        test_difficulty(args[0])
    elif opts.info:
        whatis()
    else:                               # solve
        if len(args) == 2:
            status = not solve_sudoku(args[0], args[1])
        else:
            status = not solve_sudoku(args[0])

    sys.exit(status)

if __name__ == "__main__":
    try:
        main()
    except (SystemExit, KeyboardInterrupt):
        pass
    except:
        print >> sys.stderr, _(u"\t-- cut here --")
        traceback.print_exc()
        print >> sys.stderr, _(u"\t-- cut here --")
        print >> sys.stderr
        print >> sys.stderr, _(u"An error has happened, please go to %s and send a bug report with the last lines.") % (BUG_URL)
